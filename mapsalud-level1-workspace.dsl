workspace {

    model {
        user = person "User" "Empleado"
        userManager = person "Gestor" "RRHH, administradores"
        
        softwareSystem = softwareSystem "Software System" "Mapsalud system"
        /* dbSystem = softwareSystem "Database" "Base de datos de la aplicación" {
            tags = "Database"
        }
        storageSystem = softwareSystem "Storage System" "Cloud Storage" {
            tags = "External System"
        }*/
        externalSystem = softwareSystem "External System" "Sistemas de las empresas" {
            tags = "External System"
        }

        user -> softwareSystem "Uses"
        userManager -> softwareSystem "Uses"
        //softwareSystem -> dbSystem "Consume (read & write)"
        //softwareSystem -> storageSystem "Store"
        softwareSystem ->  externalSystem "Interact"
        
    }

    views {
        systemContext softwareSystem "SystemContext" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #A0A4A9
                color #ffffff
            }
            element "Database" {
                background #A0A4A9
                color #ffffff
                shape cylinder
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
        }
    }
    
}