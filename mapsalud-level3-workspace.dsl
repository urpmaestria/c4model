workspace {
    
    model {
        
        mapsaludSystem = softwareSystem "Software System" {

            apiApp = container "API Application" {
                signInc = component "Sign In Controller"
                customerCtrl = component "Customer Controller"
                userCtrl = component "User Controller"
                supplierCtrl = component "Suppliers Controller"
                benefitCtrl = component "Benefits Controller"
                medicSpeCtrl = component "Medics and Specialties Controller"
                controller2 = component "Controller 2"

                securityComp = component "Security Component"
                component3 = component "Customer Component"
                userComp = component "User Component"
                supplierComp = component "Suppliers Component"
                benefitComp = component "Benefits Component"
                medicSpeComp = component "Medics and Specialties Component"
                component2 = component "Component 2"
            }

            webApp = container "Web Application" "Javascript - AngularJs" {
                tags = "Software System"
            }
            iosApp = container "Mobile Android Application" "Android SDK - Native"  {
                tags = "Software System"
            }
            mobileApp = container "Mobile iOs Application" "iOs SDK - Native" {
                tags = "Software System"
            }

            dbSystem = container "Database" {
                tags = "Database"
            }

            storageSystem = container "Storage System" "Cloud Storage" {
                tags = "External System"
            }
        }
       
        externalSystem = softwareSystem "External System" "Sistemas de las empresas" {
            tags = "External System"
        }
    
        signInc -> securityComp "Uses"
        controller2 -> component2 "Uses"
        controller2 -> securityComp "Uses"
        customerCtrl -> component3 "Uses"
        customerCtrl -> securityComp "Uses"
        userCtrl -> userComp "Uses"
        userCtrl -> securityComp "Uses"
        supplierCtrl -> supplierComp "Uses"
        supplierCtrl -> securityComp "Uses"
        medicSpeCtrl -> medicSpeComp "Uses"
        medicSpeCtrl -> securityComp "Uses"
        benefitCtrl -> benefitComp "Uses"
        benefitCtrl -> securityComp "Uses"
        
        mobileApp -> signInc "Makes API calls(JSON/HTTPS)"
        mobileApp -> controller2 "Makes API calls(JSON/HTTPS)"
        mobileApp -> userCtrl "Makes API calls(JSON/HTTPS)"
        mobileApp -> supplierCtrl "Makes API calls(JSON/HTTPS)"
        mobileApp -> medicSpeCtrl "Makes API calls(JSON/HTTPS)"
        mobileApp -> benefitCtrl "Makes API calls(JSON/HTTPS)"
        mobileApp -> customerCtrl "Makes API calls(JSON/HTTPS)"

        iosApp -> signInc "Makes API calls(JSON/HTTPS)"
        iosApp -> controller2 "Makes API calls(JSON/HTTPS)"
        iosApp -> userCtrl "Makes API calls(JSON/HTTPS)"
        iosApp -> supplierCtrl "Makes API calls(JSON/HTTPS)"
        iosApp -> medicSpeCtrl "Makes API calls(JSON/HTTPS)"
        iosApp -> benefitCtrl "Makes API calls(JSON/HTTPS)"
        iosApp -> customerCtrl "Makes API calls(JSON/HTTPS)"

        webApp -> signInc "Makes API calls(JSON/HTTPS)"
        webApp -> controller2 "Makes API calls(JSON/HTTPS)"
        webApp -> userCtrl "Makes API calls(JSON/HTTPS)"
        webApp -> supplierCtrl "Makes API calls(JSON/HTTPS)"
        webApp -> medicSpeCtrl "Makes API calls(JSON/HTTPS)"
        webApp -> benefitCtrl "Makes API calls(JSON/HTTPS)"
        webApp -> customerCtrl "Makes API calls(JSON/HTTPS)"

        securityComp -> dbSystem "Consume (read & write) / HTTPS"
        userComp -> dbSystem "Consume (read & write) / HTTPS"
        supplierComp -> dbSystem "Consume (read & write) / HTTPS"
        benefitComp -> dbSystem "Consume (read & write) / HTTPS"
        medicSpeComp -> dbSystem "Consume (read & write) / HTTPS"
        //component3 -> dbSystem "Consume (read & write) / HTTPS"
        
        component2 -> dbSystem "Consume (read & write) / HTTPS"
        component2 -> storageSystem "Consume (read & write) /HTTPS"
        component3 -> externalSystem  "Interact (JSON/XML HTTPS)"
        
    }

    views {
        component apiApp {
            include *
            autoLayout tb
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #A0A4A9
                color #ffffff
            }
            element "Database" {
                background #A0A4A9
                color #ffffff
                shape cylinder
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
        }
    }
    
}