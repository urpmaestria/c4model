workspace {

    model {
        user = person "User" "Empleado"
        userManager = person "Gestor" "RRHH, administradores"
        
        mapsaludSystem = softwareSystem "Software System" {
            
            webApp = container "Web Application" "Javascript - AngularJs" {
                tags = "Software System"
            }
            iosApp = container "Mobile Android Application" "Android SDK - Native"  {
                tags = "Software System"
            }
            mobileApp = container "Mobile iOs Application" "iOs SDK - Native" {
                tags = "Software System"
            }

            #iosApp = container "Mobile Application" "iOs SDK(Native)" {
            #    tags = "Software System"
            #}
            apiApp = container "API Application" "Typescript - NodeJs / Express" {
                tags = "Software System"
            }
            dbSystem = container "Database" {
                tags = "Database"
            }

            storageSystem = container "Storage System" "Cloud Storage" {
                tags = "External System"
            }
        }
       
        externalSystem = softwareSystem "External System" "Sistemas de las empresas" {
            tags = "External System"
        }

        user -> mobileApp "Uses"
        user -> iosApp "Uses"
        user -> webApp "Uses (HTTPS)"
        userManager -> webApp "Uses (HTTPS)"
        mobileApp -> apiApp "Makes API calls(JSON/HTTPS)"
        iosApp -> apiApp "Makes API calls(JSON/HTTPS)"
        webApp -> apiApp "Makes API calls(JSON/HTTPS)"
        apiApp -> dbSystem "Consume (read & write) / HTTPS"
        apiApp -> storageSystem "Consume (read & write) /HTTPS"
        apiApp -> externalSystem  "Interact (JSON/XML HTTPS)"
        
    }

    views {
        container mapsaludSystem "Containers_All" {
            include *
            autolayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #A0A4A9
                color #ffffff
            }
            element "Database" {
                background #A0A4A9
                color #ffffff
                shape cylinder
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
        }
    }
    
}